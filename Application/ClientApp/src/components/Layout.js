import React from "react";
import { Col, Grid } from "react-bootstrap";

export default props => (
	<Grid fluid>
		<Col sm={12}>{props.children}</Col>
	</Grid>
);
