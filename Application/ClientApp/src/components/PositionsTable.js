﻿import React, { Component } from "react";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import { imgFormatter } from "./ImgFormatter";
import { linkFormatter } from "./LinkFormatter";
import "react-bootstrap-table/css/react-bootstrap-table.css";

class PositionsTable extends Component {
    constructor(props) {
        super(props);

        this.options = {
            defaultSortName: "title",
            defaultSortOrder: "asc"
        };
        this.state = { width: 0, height: 0 };
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener("resize", this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    render() {
        const { positions } = this.props;
        const { width } = this.state;
        const hideIconViewPort = 500;
        return (
            <div>
                <BootstrapTable
                    options={this.options}
                    ref="table"
                    data={positions}
                    pagination
                >
                    <TableHeaderColumn dataField="id" isKey hidden />
                    <TableHeaderColumn dataField="title" dataSort>
                        Title
                    </TableHeaderColumn>
                    <TableHeaderColumn dataField="location" dataSort>
                        Location
                    </TableHeaderColumn>
                    <TableHeaderColumn
                        dataField="company_link"
                        dataFormat={linkFormatter}
                        dataSort
                    >
                        Company
                    </TableHeaderColumn>
                    {width > hideIconViewPort && (
                        <TableHeaderColumn
                            dataField="company_logo"
                            dataFormat={imgFormatter}
                        >
                            Comapny Logo
                        </TableHeaderColumn>
                    )}
                </BootstrapTable>
            </div>
        );
    }
}

export default PositionsTable;
