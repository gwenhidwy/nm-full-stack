﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using NMProject.Models;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace NMProject.Service
{
    public class PositionService : IApiService<Position>
    {
        private readonly IConfiguration _configuration;
        private static readonly HttpClient client = new HttpClient();
        public PositionService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<IEnumerable<Position>> Search(string location)
        {
            StringBuilder url = new StringBuilder();
            url.Append(_configuration["ApiInfo:ApiUrl"]);
            url.Append($"?location={location}");
            string urlString = url.ToString();
            string result = await ApiCall(urlString);
            IEnumerable<Position> positions = JsonConvert.DeserializeObject<IEnumerable<Position>>(result);
            foreach (Position position in positions)
            {
                position.Title = position.Title.Trim();
                position.Location = position.Location.Trim();
                position.Company = position.Company.Trim();
            }
            return positions;
        }
        private static async Task<string> ApiCall(string url)
        {
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
            client.DefaultRequestHeaders.Add("User-Agent", "Github Job Posting");
            Task<string> stringTask = client.GetStringAsync(url);

            string result = await stringTask;
            return result;
        }
    }
}
