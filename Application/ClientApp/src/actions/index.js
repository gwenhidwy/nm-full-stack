﻿import * as api from "../api";
import * as actionTypes from "./actionTypes";

export const requestPositions = search => (dispatch, getState) => {
	if (search === getState().positions.searchValue) {
		return;
	}

	dispatch({ type: actionTypes.requestPositionsType, search });

	api.fetchPositions(search).then(response =>
		dispatch({ type: actionTypes.receivePositionsType, search, response })
	);
};

export const filterPositions = val => dispatch => {
	const { filter = "" } = val;
	dispatch({ type: actionTypes.filterPositionsType, filter });
};
