﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace NMProject.Service
{
    public interface IApiService<T> where T : class
    {

       Task<IEnumerable<T>> Search(string search);
    }
}
