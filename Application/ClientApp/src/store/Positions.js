﻿import * as actionTypes from "../actions/actionTypes";

const initialState = {
    positions: [],
    filteredPositions: [],
    isLoading: false
};

const createCompanyLink = position => ({
    ...position,
    ...{
        company_link: {
            name: position.company,
            url: position.company_url
        }
    }
});
const doesContain = (item, action) =>
    item.toLowerCase().includes(action.filter.toLowerCase());

export const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.requestPositionsType:
            return {
                ...state,
                searchValue: action.search,
                isLoading: true
            };
        case actionTypes.receivePositionsType:
            return {
                ...state,
                searchValue: action.search,
                positions: action.response.map(position =>
                    createCompanyLink(position)
                ),
                filteredPositions: action.response.map(position =>
                    createCompanyLink(position)
                )
            };
        case actionTypes.filterPositionsType:
            return {
                ...state,
                filteredPositions: state.positions.filter(
                    ({ title, location, company }) =>
                        doesContain(title, action) ||
                        doesContain(location, action) ||
                        doesContain(company, action)
                )
            };
        default:
            return state;
    }
};
