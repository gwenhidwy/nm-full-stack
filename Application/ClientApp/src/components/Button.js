﻿import React from "react";
import './index.css'

const Button = ({ type = "button", className = '', children, ...restProps }) => (
    <button
        className={`form__button ${className}`}
        type={type}
        {...restProps}>
        {children}
    </button>
)

export default Button;