﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NMProject.Models;
using NMProject.Service;

namespace Application.Controllers
{
    [Route("api/[controller]")]
    public class PositionsController : Controller
    {
        private readonly IApiService<Position> _service;
        public PositionsController(IApiService<Position> service)
        {
            _service = service;
        }
        // GET: Positions
        [HttpGet("[action]")]
        public async Task<IEnumerable<Position>> Index(string location)
        {
            IEnumerable<Position> positions = await _service.Search(location);
            return positions;
        }
    }   
}