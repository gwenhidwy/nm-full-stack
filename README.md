# Northwestern Mutual Full Stack Project

### I created a sortable table of Github job postings with a real-time filter

* It was implimented using .NET Core 2.1 with React and Redux.
* The .NET portion of the project uses dependency injection for a service that accesses the Github Api.
* The React-Redux fronted utilizes redux-form and react-bootstrap-table.