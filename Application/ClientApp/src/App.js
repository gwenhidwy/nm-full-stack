﻿import React from "react";
import { Route } from "react-router";
import Layout from "./components/Layout";
import Positions from "./components/Positions";

export default () => (
	<Layout>
		<Route path="/" component={Positions} />
	</Layout>
);
