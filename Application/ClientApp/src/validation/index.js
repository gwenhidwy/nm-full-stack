﻿export const isZipOrCity = value => {
	const isZipOrText = RegExp("d{5}|[a-z -']+", "i");
	return !!value
		? isZipOrText.test(value)
			? undefined
			: "Value must be zip code or city"
		: undefined;
};
