﻿import React, { Component } from "react";
import LocationForm from "./LocationForm";
import FilterForm from "./FilterForm";
import VisiblePositionsTable from "./VisiblePositionsTable";
import './Positions.css';

class Positions extends Component {
    constructor(props) {
        super(props);
        this.state = { search: "", filter: "" };
    }

    handleFormSubmit = values => {
        const { location = '' } = values;
        this.setState({ search: location });
    };

    handleFormChange = values => {
        this.setState({ filter: values });
    };

    render() {
        const { search, filter } = this.state;
        return (
            <div>
                <h1>Github Job Postings</h1>
                <LocationForm
                    onSubmit={this.handleFormSubmit}
                />
                <FilterForm
                    onChange={this.handleFormChange}
                />
                <VisiblePositionsTable
                    search={search}
                    filter={filter}
                />
            </div>
        );
    }
}

export default Positions;
