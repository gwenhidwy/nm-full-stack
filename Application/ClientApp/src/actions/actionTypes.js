﻿export const requestPositionsType = "REQUEST_POSITIONS";
export const receivePositionsType = "RECEIVE_POSITIONS";
export const filterPositionsType = "FILTER_POSITIONS";
