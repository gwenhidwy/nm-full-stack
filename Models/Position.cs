﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace NMProject.Models
{
    [Serializable]
    public class Position
    {
        [JsonProperty(PropertyName = "id")]
        public Guid Id { get; set; }
        [JsonProperty(PropertyName = "created_at")]
        [JsonConverter(typeof(CustomDateTimeConverter))]
        public DateTime CreatedAt { get; set; }
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }
        [JsonProperty(PropertyName = "location")]
        public string Location { get; set; }
        [JsonProperty(PropertyName = "description")]
        public string Descritpion { get; set; }
        [JsonProperty(PropertyName = "how_to_apply")]
        public string HowToApply { get; set; }
        [JsonProperty(PropertyName = "company")]
        public string Company { get; set; }
        [JsonProperty(PropertyName = "company_url")]
        public string CompanyUrl { get; set; }
        [JsonProperty(PropertyName = "company_logo")]
        public string CompanyLogo { get; set; }
        [JsonProperty(PropertyName = "url")]
        public string Url { get; set; }
    }
}
