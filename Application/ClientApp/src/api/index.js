﻿const API_URL = "api/Positions/Index?location=";

export const fetchPositions = search => {
	const url = `${API_URL}${search}`;
	return fetch(url).then(response => {
		return response.json();
	});
};
