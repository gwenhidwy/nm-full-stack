﻿import React from "react";
import { Field, reduxForm } from "redux-form";
import { isZipOrCity } from '../validation';
import CustomInput from './CustomInput';

let LocationForm = ({ handleSubmit, pristine, reset, submitting }) => {
    return (
        <form onSubmit={handleSubmit}>
            <div>
                <Field
                    name="location"
                    component={CustomInput}
                    type="text"
                    label="City or Zip Code"
                    validate={[isZipOrCity]}
                />
                <button
                    type="submit"
                    disabled={submitting}
                >
                    Submit
                </button>
                <button
                    type="button"
                    disabled={pristine || submitting}
                    onClick={reset}
                >
                    Clear
                </button>
            </div>
        </form>
    );
};

export default (LocationForm = reduxForm({
    form: "locationForm"
})(LocationForm));
