﻿import React from "react";
import './index.css'

const ImgFormatter = ({ img }) => {
	return <img src={img} alt="" className="image" />;
}

export const imgFormatter = (cell, row) => {
	return <ImgFormatter img={cell} />;
};
