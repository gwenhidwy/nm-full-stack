﻿import React from "react";

const LinkFormatter = ({ link, text }) => {
	return link ? <a href={link}>{text}</a> : text;
};

export const linkFormatter = (cell, row) => {
	return <LinkFormatter link={cell.url} text={cell.name} />;
};
