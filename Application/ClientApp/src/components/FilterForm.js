﻿import React from "react";
import { Field, reduxForm } from "redux-form";
import CustomInput from './CustomInput';

let FilterForm = ({ handleChange, pristine, reset, submitting }) => {
    return (
        <form onChange={handleChange}>
            <div>
                <Field
                    name="filter"
                    component={CustomInput}
                    type="text"
                    label="Search within"
                />
                <button
                    type="button"
                    disabled={pristine || submitting}
                    onClick={reset}>
                    Clear
                </button>
            </div>
        </form>
    );
};

export default (FilterForm = reduxForm({
    form: "filterForm"
})(FilterForm));
