﻿import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../actions";
import { bindActionCreators } from "redux";
import PositionsTable from "./PositionsTable";

class VisiblePositionsTable extends Component {
    componentDidMount() {
        this.fetchData();
    }

    componentDidUpdate(prevProps) {
        if (this.props.search !== prevProps.search) {
            this.fetchData();
        }
        if (this.props.filter !== prevProps.filter) {
            this.getFilteredPositions();
        }
    }

    getFilteredPositions() {
        const { filter, filterPositions } = this.props;
        filterPositions(filter);
    }

    fetchData() {
        const { search, requestPositions } = this.props;
        requestPositions(search);
    }

    render() {
        const { filteredPositions } = this.props;
        return <PositionsTable positions={filteredPositions} />;
    }
}

export default VisiblePositionsTable = connect(
    state => state.positions,
    dispatch => bindActionCreators(actions, dispatch)
)(VisiblePositionsTable);
